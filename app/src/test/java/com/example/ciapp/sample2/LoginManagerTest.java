package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Password1");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "Password1");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Password1"));
    }

    @Test(expected = LoginFailedException.class)
    public void testPasswordwrong1()throws LoginFailedException{
        User user = loginManager.login("testuser1","password");

    }

    @Test(expected = LoginFailedException.class)
    public void testPasswordwrong2()throws LoginFailedException{
        User user = loginManager.login("testuser1","pass");

    }



    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("iniad", "password");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUsername1() throws LoginFailedException {
        User user = loginManager.login("testuser1", "Test1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUsername2() throws LoginFailedException {
        User user = loginManager.login("testuser", "Test1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUsername3() throws LoginFailedException {
        User user = loginManager.login("1234", "Test1234");
    }
    @Test
    public void testLoginUsername4() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "Password1");
        assertThat(user.getUsername(), is("Testuser1"));
    }

}